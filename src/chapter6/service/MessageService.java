package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String start, String end) {
        final int LIMIT_NUM = 1000;
        final String DEFFAULT_START = "2020/01/01 00:00:00";
        final String DEFFAULT_END = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());;

        Connection connection = null;
        try {
            connection = getConnection();

            Integer id = null;
            if(!StringUtils.isEmpty(userId)) {
            	id = Integer.parseInt(userId);
            }

            String startDate;
            String endDate;

            if (StringUtils.isNotBlank(start)) {
            	startDate = start + " 00:00:00";
            } else {
            	startDate = DEFFAULT_START;
            }

            if (StringUtils.isNotBlank(end)) {
            	endDate = end + " 23:59:59";
            } else {
            	endDate = DEFFAULT_END;
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, id, startDate, endDate, LIMIT_NUM);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public Message select(int messageId) {

    	Connection connection = null;
    	try {
            connection = getConnection();

            Message messages = new MessageDao().select(connection, messageId);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(Message message) {

    	Connection connection = null;
    	try {
            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);
    	} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(int id) {
    	Connection connection = null;
    	try {
            connection = getConnection();
            new MessageDao().delete(connection, id);
            commit(connection);
    	} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}