package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();
		String messageIdParameter = request.getParameter("id");

		if (messageIdParameter != null && messageIdParameter.matches("^[0-9]+$")) {
				int messageId = Integer.parseInt(messageIdParameter);
				Message message = new MessageService().select(messageId);
				if (message.getId() != 0) {
					request.setAttribute("message", message);
					request.getRequestDispatcher("edit.jsp").forward(request, response);
					return;
				} else {
					errorMessages.add("不正なパラメーターが入力されました");
				}
		} else {
			errorMessages.add("不正なパラメーターが入力されました");
		}

		if (errorMessages.size() != 0) {
			session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
		}
    }

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();

		Message message = getMessage(request);
		if (isValid(message, errorMessages)) {
			new MessageService().update(message);
        }

        if (errorMessages.size() != 0) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("message", message);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
        }

        response.sendRedirect("./");
	}

	public Message getMessage(HttpServletRequest request) {
		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("id")));
		message.setText(request.getParameter("text"));

		return message;
	}

	private boolean isValid(Message message, List<String> errorMessages) {

		String text = message.getText();

		if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
	}

}
